package belajaroop.dasar;

public class KelasSmartWatch extends KelasKalkulator{ 
	//kelas smartwatch sudah bisa mengakses
	//fungsi atau method dari kelas kalkulator

	public void cuaca() {
		String suhu = "Sejuk";
		System.out.println(suhu);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		KelasSmartWatch ks = new KelasSmartWatch();
		ks.cuaca();
		ks.tambah(3, 548);
		ks.kurang(500, 200);
		
	}

}
