package belajaroop.dasar;

public class KelasArray {

	public static void main(String[] args) {
		
		//array 1 dimensi//
		String[] buah = new String[3];
		buah[0] = "Pisang";
		buah[1] = "Salak";
		buah[2] = "Nanas";
		
		//array 2 dimensi//
		String[][] makanan = new String[3][2];
		makanan[0][0] = "Pisang Raja";
		makanan[0][1] = "Pisang Ambon";
		
		makanan[1][0] = "Salak Pondoh";
		makanan[1][0] = "Salak Kampung";
		
		
		makanan[2][0] = "Nanas Subang";
		makanan[2][1] = "Nanas Madu";
		
		System.out.println(buah);
		System.out.println(makanan);
		System.out.println();
	}
	
}
