package belajaroop.dasar;

public class KelasMethodPerumahanSebelah {

	// instance class
	// yaitu memanggil / menggunakan class lain
	// fungsi nya untuk memakai class sebelumnya
	
	// syntax : NamaKelasYangDipanggil nickname = new NamaKelasYangDipanggil();
	
	
	static void panggil() {
		KelasMethodPerumahan kmp = new KelasMethodPerumahan();
		kmp.taman();
		kmp.gedungAula();
	}
}
