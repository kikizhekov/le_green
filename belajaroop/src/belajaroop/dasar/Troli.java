package belajaroop.dasar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Troli {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Array List
		
		//Array ada dua jenis:
		//1 Menggunakan List => Sering dipakai
		//2 Menggunakan Collection
		
		List<Sayuran> sList = new ArrayList<Sayuran>();
		//syntax untuk membuat list yang banyak dari array sebuah class
		Sayuran sayur1 = new Sayuran();
		sayur1.setNamaSayur("Kangkung");
		sayur1.setHargaSayur(3000);
		
		sList.add(sayur1);
		
		Sayuran sayur2 = new Sayuran();
		sayur2.setNamaSayur("Bayam");
		sayur2.setHargaSayur(2000);
		
		sList.add(sayur2);
		
		Sayuran sayur3 = new Sayuran();
		sayur3.setNamaSayur("Katuk");
		sayur3.setHargaSayur(4000);
		
		sList.add(sayur3);
		
		for (int i = 0; i < 3; i++) {
			System.out.println(sList.get(i).getNamaSayur());
			System.out.println(sList.get(i).getHargaSayur());
		}
	}

}
