package belajaroop.dasar;

import java.util.ArrayList;
import java.util.List;

public class IsiBiodata {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Biodata> biodataList = new ArrayList<Biodata>();
		
		Biodata biodata1 = new Biodata();
		biodata1.setNamaDepan("Muhammad");
		biodata1.setNamaBelakang("Jaenudin");
		biodata1.setTinggi(170);
		biodata1.setBeratBadan(60);
		
		biodataList.add(biodata1);
		
		Biodata biodata2 = new Biodata();
		biodata2.setNamaDepan("Siti");
		biodata2.setNamaBelakang("Humairoh");
		biodata2.setTinggi(166);
		biodata2.setBeratBadan(67);
		
		biodataList.add(biodata2);
	}

}
