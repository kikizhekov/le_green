package belajaroop.dasar;

public class KelasMethodIsi {
	
	public void isiNama(String namaDepan) {
		String namaLengkap = "Nabila";
		System.out.println(namaDepan + namaLengkap);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		KelasMethodIsi kmi = new KelasMethodIsi();
		String namaDepan = "Fitri";
		
		kmi.isiNama(namaDepan);
	}

}
