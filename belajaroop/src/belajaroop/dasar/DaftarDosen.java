package belajaroop.dasar;

import java.util.ArrayList;
import java.util.List;

public class DaftarDosen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Dosen> dosenList = new ArrayList<Dosen>();
		
		Dosen dosen1 = new Dosen();
		dosen1.setNamaDosen("Prof. Spongebob");
		dosen1.setUsiaDosen(70);
		dosenList.add(dosen1);
		
		Dosen dosen2 = new Dosen();
		dosen2.setNamaDosen("Prof. Dr. Patrick");
		dosen2.setUsiaDosen(69);
		dosenList.add(dosen2);
		
		System.out.println(dosenList.get(1).getNamaDosen());
		System.out.println(dosenList.get(0).getUsiaDosen());
		
		for (int i = 0; i < dosenList.size(); i++) {
			System.out.println(dosenList.get(0).getNamaDosen());
		}
	}

}
