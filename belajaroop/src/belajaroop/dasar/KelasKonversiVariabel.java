package belajaroop.dasar;

public class KelasKonversiVariabel {

	double komaSatu = 9.1;
	int bulatDua = 8;
	
	/* int hasil = komaSatu + bulatDua; */
	
	int hasil = (int) komaSatu + bulatDua;
	
	/* Mengubah / konversi Tipe Data */
	// 1. (tipeData) nilai
	// 2. kelasTipeData.valueOf(nilai)
	
	String hurufBulatDua = String.valueOf(bulatDua);
}
