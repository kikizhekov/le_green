package belajaroop.dasar;

public class Sayuran {

	
	private String namaSayur;
	private int hargaSayur;
	
	//POJO = Plain Old Java Object : Array yang memiliki satu / lebih tipe data yang berbeda
	//1 atribut getter
	//2 atribut setter
	
	public String getNamaSayur() {
		return namaSayur;
	}
	public void setNamaSayur(String namaSayur) {
		this.namaSayur = namaSayur;
	}
	public int getHargaSayur() {
		return hargaSayur;
	}
	public void setHargaSayur(int hargaSayur) {
		this.hargaSayur = hargaSayur;
	}
	
}
