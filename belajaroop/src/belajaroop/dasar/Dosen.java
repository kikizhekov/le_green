package belajaroop.dasar;

public class Dosen {

	
	private String namaDosen;
	private int usiaDosen;
	
	public String getNamaDosen() {
		return namaDosen;
	}
	public void setNamaDosen(String namaDosen) {
		this.namaDosen = namaDosen;
	}
	public int getUsiaDosen() {
		return usiaDosen;
	}
	public void setUsiaDosen(int usiaDosen) {
		this.usiaDosen = usiaDosen;
	}
}
