package belajaroop.dasar;

public interface Komputer {

	// ini method biasa //
	
	/*
	 * public void browsing() { 
	 * String a = "halo"; 
	 * System.out.println("HI"); 
	 * }
	 */
	
	// ini method//
	
	
	// ini method abstract //
	// abstract adalah tak berisi / berbentuk //
	
	public void browsing();
	// ini method abstract //
	
}
