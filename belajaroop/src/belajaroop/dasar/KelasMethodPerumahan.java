package belajaroop.dasar;

public class KelasMethodPerumahan {

	public void taman() {
		
	}
	
	private void kolamRenang() {
		
	}
	
	protected void gedungAula() {
		
	}
	
	static void panggil() {
		KelasMethodPerumahan kmp = new KelasMethodPerumahan();
		kmp.taman();
		kmp.gedungAula();
		kmp.kolamRenang();
	}
}
