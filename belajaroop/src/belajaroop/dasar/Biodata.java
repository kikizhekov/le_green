package belajaroop.dasar;

public class Biodata {

	
	private String namaDepan;
	private String namaBelakang;
	private int tinggi;
	private int beratBadan;
	
	public String getNamaDepan() {
		return namaDepan;
	}
	public void setNamaDepan(String namaDepan) {
		this.namaDepan = namaDepan;
	}
	public String getNamaBelakang() {
		return namaBelakang;
	}
	public void setNamaBelakang(String namaBelakang) {
		this.namaBelakang = namaBelakang;
	}
	public int getTinggi() {
		return tinggi;
	}
	public void setTinggi(int tinggi) {
		this.tinggi = tinggi;
	}
	public int getBeratBadan() {
		return beratBadan;
	}
	public void setBeratBadan(int beratBadan) {
		this.beratBadan = beratBadan;
	}
	
	
}
