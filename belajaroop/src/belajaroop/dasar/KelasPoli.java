package belajaroop.dasar;

public class KelasPoli {

	// Polimorfisme Tipe Overloading //
	
	public void gabung(int inputA, int inputB) {
		
		int hasil = inputA + inputB;
		System.out.println(hasil);
	}
	
	public void gabung(String inputA, String inputB) {

		String hasil = inputA + inputB;
		System.out.println(hasil);
	}
	
	public void gabung() {
		System.out.println("Method Gabung");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		KelasPoli kp = new KelasPoli();
		kp.gabung(3000, 2000);
		kp.gabung("Kiki ", "Ganteng");
		kp.gabung();
	}

}
