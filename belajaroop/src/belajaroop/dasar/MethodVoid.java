package belajaroop.dasar;

public class MethodVoid {

	public void cetak() {
		System.out.println("Sysout println :v");
	}
	
	public int kursDollar() {
		int satuDollar = 15000;
		return satuDollar;
	}
	
	public String experience() {
		String posisi = "Jadi IT Kayak Ndolor";
		return posisi;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MethodVoid kmv = new MethodVoid();
		kmv.cetak();
		
		int uangDollar = 17;
		int satuDollar = kmv.kursDollar();
		int totalRupiah = uangDollar * satuDollar;
		System.out.println(totalRupiah);
		
		String nama = "Aris";
		String posisi = kmv.experience();
		String cv = nama + " " + posisi;
		System.out.println(cv);
	}
	

}
