package belajaroop.dasar;

public class KelasVariabel {

	int angka = 9; 
	// ini angka bulat
	// angka tengah 0
	// 2,14M
	
	long angkaPanjang = 9;
	// 1000Triliun
	
	char karakter = '0';
	
	double koma = 9.5;
	// 7 angka di belakang koma
	
	float komaPanjang = 9.2f;
	// bisa 7 - 14 angka di belakang koma
	
	String huruf = "Laptop";
	// char hanya satu karakter dan pakai petik satu ( '' )
	// String bisa lebih dari satu karakter dan pakai petik dua ( " " )
	
	boolean cek = true;
}
