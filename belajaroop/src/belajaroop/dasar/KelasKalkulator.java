package belajaroop.dasar;

public class KelasKalkulator {

	public void tambah(int a, int b) {
		int hasil = a + b;
		System.out.println(hasil);
	}
	
	public void kali(int a, int b) {
		int hasil = a * b;
		System.out.println(hasil);
	}
	
	public void kurang(int a, int b) {
		int hasil = a - b;
		System.out.println(hasil);
	}
}
