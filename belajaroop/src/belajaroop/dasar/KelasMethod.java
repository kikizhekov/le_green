package belajaroop.dasar;

public class KelasMethod {

	
	// syntax method dasar //
	// static bisa diganti dengan tiga akses modifier :
	
	// 1. Public
	// 2. Private
	// 3. Protected
	
	// void artinya kosong / tidak ada hasil akhir
	
	// nama method = camelName
	// lalu ditempel dengan " () "
	// fungsi " () " gunanya sbg tempat variabel
	
	static void namaMethod() {
		
	}
	// syntax method dasar //
}
