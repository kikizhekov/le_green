package belajaroop.latihan;

import java.util.ArrayList;
import java.util.List;

public class DaftarKarakter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		List<Karakter> karakterList = new ArrayList<Karakter>();
		
		Karakter karakter1 = new Karakter();
		karakter1.setNama("Ironman");
		karakter1.setLevel(1);
		karakter1.setStatus("Hero");
		
		karakterList.add(karakter1);
		
		Karakter karakter2 = new Karakter();
		karakter2.setNama("Thor");
		karakter2.setLevel(2);
		karakter2.setStatus("Hero");
		
		karakterList.add(karakter2);
		
		Karakter karakter3 = new Karakter();
		karakter3.setNama("Thanos");
		karakter3.setLevel(3);
		karakter3.setStatus("Villain");
		
		karakterList.add(karakter3);
		
		Karakter karakter4 = new Karakter();
		karakter4.setNama("Loki");
		karakter4.setLevel(4);
		karakter4.setStatus("Villain");
		
		karakterList.add(karakter4);
	}

}
