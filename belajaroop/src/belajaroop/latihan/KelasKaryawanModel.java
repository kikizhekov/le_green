package belajaroop.latihan;

public class KelasKaryawanModel {

	
	private int No;
	private String namaPegawai;
	private int gajiPegawai;
	private String status;
	
	public int getNo() {
		return No;
	}
	public void setNo(int no) {
		No = no;
	}
	public String getNamaPegawai() {
		return namaPegawai;
	}
	public void setNamaPegawai(String namaPegawai) {
		this.namaPegawai = namaPegawai;
	}
	public int getGajiPegawai() {
		return gajiPegawai;
	}
	public void setGajiPegawai(int gajiPegawai) {
		this.gajiPegawai = gajiPegawai;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
